import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { IngredientModule } from './ingredient/ingredient.module';
import { PromotionModule } from './promotion/promotion.module';
import { MemberModule } from './member/member.module';
import { Member } from './member/entities/member.entity';
import { BranchModule } from './branch/branch.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Branch } from './branch/entities/branch.entity';
import { Ingredient } from './ingredient/entities/ingredient.entity';
import { DataSource } from 'typeorm';
import { Promotion } from './promotion/entities/promotion.entity';
import { Product } from './product/entities/product.entity';
import { Type } from './types/entities/type.entity';
import { ProductModule } from './product/product.module';
import { History } from './history/entities/history.entity';
import { User } from './users/entities/user.entity';
import { OrderIngredient } from './order-ingredient/entities/order-ingredient.entity';
import { OrderIngredientItem } from './order-ingredient/entities/orderItem-ingredient.entity';
import { IngredientStore } from './ingredient-store/entities/ingredient-store.entity';
import { OrderIngredientModule } from './order-ingredient/order-ingredient.module';
import { IngredientStoreModule } from './ingredient-store/ingredient-store.module';
import { AuthModule } from './auth/auth.module';
import { OrderItem } from './order/entities/orderItem.entity';
import { Order } from './order/entities/order.entity';
import { Employee } from './employees/entities/employee.entity';
import { EmployeeModule } from './employees/employee.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { HistoryModule } from './history/history.module';
import { ReportCheckStockModule } from './report-check-stock/report-check-stock.module';

import { CheckInOutModule } from './check-in-out/check-in-out.module';
import { CheckInOut } from './check-in-out/entities/check-in-out.entity';

import { Role } from './roles/entities/role.entity';
import { RolesModule } from './roles/roles.module';
import { TypesModule } from './types/types.module';
import { UsersModule } from './users/users.module';
import { ReportIngredientStoreModule } from './report-ingredient-store/report-ingredient-store.module';
import { SalaryPaymentDetailModule } from './salary-payment-detail/salary-payment-detail.module';
import { SalaryPaymentDetail } from './salary-payment-detail/entities/salary-payment-detail.entity';
import { SalaryPaymentDetailItem } from './salary-payment-detail/entities/salary-payment-detail-item.entity';

@Module({
  imports: [
    //     TypeOrmModule.forRoot({
    //       type: 'sqlite',
    //       database: 'Work_group2_DB.sqlite',
    //       synchronize: true,
    //       entities: [
    //         Branch,
    //         Ingredient,
    //         Promotion,
    //         Member,
    //         Order,
    //         OrderItem,
    //         Product,
    //         Type,
    //         History,
    //         User,
    //         OrderIngredient,
    //         OrderIngredientItem,
    //         IngredientStore,
    //         Employee,
    //       ],
    //     }),
    //     ServeStaticModule.forRoot({
    //       rootPath: join(__dirname, '..', 'public'),
    //     }),
    //     IngredientModule,
    //     PromotionModule,
    //     MemberModule,
    //     BranchModule,
    //     TypeModule,
    //     ProductModule,
    //     HistoryModule,
    //     // UsersModule,
    //     OrderIngredientModule,
    //     IngredientStoreModule,
    //     AuthModule,
    //     UserXModule,
    //     EmployeeModule,
    //     ReportCheckStockModule,
    //   ],
    //   controllers: [AppController],
    //   providers: [AppService],
    // })
    // export class AppModule {
    //   constructor(private dataSource: DataSource) {}
    // }
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'angsila.informatics.buu.ac.th',
      port: 3306,
      username: 'cscamp09',
      password: 's6XSMhEupD',
      database: 'cscamp09',
      entities: [
        User,
        Role,
        Type,
        Branch,
        Ingredient,
        Promotion,
        Member,
        Order,
        OrderItem,
        Product,
        History,
        OrderIngredient,
        OrderIngredientItem,
        IngredientStore,
        Employee,
        CheckInOut,
        SalaryPaymentDetail,
        SalaryPaymentDetailItem,
      ],
      synchronize: true,
    }),

    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    RolesModule,
    TypesModule,
    UsersModule,
    AuthModule,
    IngredientModule,
    PromotionModule,
    MemberModule,
    BranchModule,
    ProductModule,
    HistoryModule,
    OrderIngredientModule,
    IngredientStoreModule,
    EmployeeModule,
    ReportCheckStockModule,
    CheckInOutModule,
    ReportIngredientStoreModule,
    SalaryPaymentDetailModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
