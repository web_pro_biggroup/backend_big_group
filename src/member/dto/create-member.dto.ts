export class CreateMemberDto {
  name: string;
  tel: string;
  createdDate: Date;
  [key: string]: number | string | Date;
}
