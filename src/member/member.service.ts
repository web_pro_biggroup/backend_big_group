import { Injectable } from '@nestjs/common';
import { CreateMemberDto } from './dto/create-member.dto';
import { UpdateMemberDto } from './dto/update-member.dto';
import { Member } from './entities/member.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class MemberService {
  lastId: number = 0;
  member: Member[] = [];
  constructor(
    @InjectRepository(Member)
    private MemberRepository: Repository<Member>,
  ) {}
  create(createMemberDto: CreateMemberDto) {
    const newMember = { ...createMemberDto, id: ++this.lastId };
    return this.MemberRepository.save(newMember);
  }

  findAll() {
    return this.MemberRepository.find();
  }

  findOne(id: number) {
    return this.MemberRepository.findOneBy({ id });
  }

  findOneByTel(tel: string) {
    return this.MemberRepository.findOneBy({ tel });
  }

  async update(id: number, updateMemberDto: UpdateMemberDto) {
    await this.MemberRepository.update(id, updateMemberDto);
    const Member = await this.MemberRepository.findOneBy({ id });
    return Member;
  }

  async remove(id: number) {
    const deleteMember = await this.MemberRepository.findOneBy({ id });
    return this.MemberRepository.remove(deleteMember);
  }
}
