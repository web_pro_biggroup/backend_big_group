import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class CheckInOut {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  status: string;

  @Column()
  time: string;

  @Column({ default: false })
  checked: boolean;
}
