import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateCheckInOutDto } from './dto/create-check-in-out.dto';
import { UpdateCheckInOutDto } from './dto/update-check-in-out.dto';
import { CheckInOut } from './entities/check-in-out.entity';

@Injectable()
export class CheckInOutService {
  constructor(
    @InjectRepository(CheckInOut)
    private readonly checkInOutRepository: Repository<CheckInOut>,
  ) {}

  create(createCheckInOutDto: CreateCheckInOutDto) {
    return this.checkInOutRepository.save(createCheckInOutDto);
  }

  findAll() {
    return this.checkInOutRepository.find();
  }

  findOne(id: number) {
    return this.checkInOutRepository.findOne({ where: { id } });
  }

  update(id: number, updateCheckInOutDto: UpdateCheckInOutDto) {
    return this.checkInOutRepository.update(id, updateCheckInOutDto);
  }

  remove(id: number) {
    return this.checkInOutRepository.delete(id);
  }
}
