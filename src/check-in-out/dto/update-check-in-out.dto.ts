import { PartialType } from '@nestjs/mapped-types';
import { CreateCheckInOutDto } from './create-check-in-out.dto';

export class UpdateCheckInOutDto extends PartialType(CreateCheckInOutDto) {
  name: string;
  status: string;
  time: string;
  checked: boolean;
}
