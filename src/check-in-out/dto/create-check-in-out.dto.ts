export class CreateCheckInOutDto {
  name: string;
  status: string;
  time: string;
  checked: boolean;
}
