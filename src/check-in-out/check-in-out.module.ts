import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CheckInOutService } from './check-in-out.service';
import { CheckInOutController } from './check-in-out.controller';
import { CheckInOut } from './entities/check-in-out.entity'; // Import the entity

@Module({
  imports: [TypeOrmModule.forFeature([CheckInOut])], // Import the entity into TypeOrmModule
  controllers: [CheckInOutController],
  providers: [CheckInOutService],
})
export class CheckInOutModule {}
