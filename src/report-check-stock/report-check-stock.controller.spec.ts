import { Test, TestingModule } from '@nestjs/testing';
import { ReportCheckStockController } from './report-check-stock.controller';

describe('ReportCheckStockController', () => {
  let controller: ReportCheckStockController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ReportCheckStockController],
    }).compile();

    controller = module.get<ReportCheckStockController>(
      ReportCheckStockController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
