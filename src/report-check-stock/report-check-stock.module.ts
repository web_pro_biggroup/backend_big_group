import { Module } from '@nestjs/common';
import { ReportCheckStockController } from './report-check-stock.controller';
import { ReportCheckStockService } from './report-check-stock.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { History } from 'src/history/entities/history.entity';

@Module({
  imports: [TypeOrmModule.forFeature([History])],
  controllers: [ReportCheckStockController],
  providers: [ReportCheckStockService],
})
export class ReportCheckStockModule {}
