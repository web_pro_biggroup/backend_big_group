import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { History } from 'src/history/entities/history.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ReportCheckStockService {
  constructor(
    @InjectRepository(History)
    private historyRepository: Repository<History>,
  ) {}
  async report1(selectedDate: string) {
    const data = await this.historyRepository.query(
      `
      
  
      SELECT
      idIngredient,
      name,
      DATE_FORMAT(createdDate, '%Y-%m-%d') AS month,
      SUM(ABS(inStock - (
          SELECT inStock
          FROM history h2
          WHERE h2.idIngredient = h.idIngredient
              AND DATE_FORMAT(h2.createdDate, '%Y-%m-%d') = DATE_FORMAT(h.createdDate, '%Y-%m-%d')
              AND h2.createdDate < h.createdDate
          ORDER BY h2.createdDate DESC
          LIMIT 1
      ))) AS total_diff
  FROM history h
  WHERE DATE_FORMAT(createdDate, '%Y-%m-%d') = ?
  GROUP BY idIngredient, name, DATE_FORMAT(createdDate, '%Y-%m-%d')
  ORDER BY idIngredient, DATE_FORMAT(createdDate, '%Y-%m-%d');
  

    
    
      `,
      [selectedDate],
    );
    return data;
  }

  async report2(selectedDate: string) {
    const data = await this.historyRepository.query(
      `
      SELECT
      idIngredient,
      name,
      DATE_FORMAT(createdDate, '%Y-%m') AS month,
      SUM(diff) AS total_diff
  FROM (
      SELECT
          idIngredient,
          name,
          createdDate,
          (CASE
              WHEN inStock = 20 THEN 0
              ELSE ABS(inStock - (
                  SELECT inStock
                  FROM history h2
                  WHERE h2.idIngredient = h.idIngredient
                      AND DATE_FORMAT(h2.createdDate, '%Y-%m') = DATE_FORMAT(h.createdDate, '%Y-%m')
                      AND h2.createdDate < h.createdDate
                  ORDER BY h2.createdDate DESC
                  LIMIT 1
              ))
          END) AS diff
      FROM history h
  ) instock_diff
  WHERE DATE_FORMAT(createdDate, '%Y-%m') = ?
  GROUP BY idIngredient, name, DATE_FORMAT(createdDate, '%Y-%m')
  ORDER BY idIngredient, DATE_FORMAT(createdDate, '%Y-%m');
  

    
    `,
      [selectedDate],
    );
    return data;
  }

  async report3(selectedDate: string) {
    const data = await this.historyRepository.query(
      `
      SELECT
      idIngredient,
      name,
      DATE_FORMAT(createdDate, '%Y') AS month,
      SUM(diff) AS total_diff
  FROM (
      SELECT
          idIngredient,
          name,
          createdDate,
          (CASE
              WHEN inStock = 20 THEN 0
              ELSE ABS(inStock - (
                  SELECT inStock
                  FROM history h2
                  WHERE h2.idIngredient = h.idIngredient
                      AND DATE_FORMAT(h2.createdDate, '%Y') = DATE_FORMAT(h.createdDate, '%Y')
                      AND h2.createdDate < h.createdDate
                  ORDER BY h2.createdDate DESC
                  LIMIT 1
              ))
          END) AS diff
      FROM history h
  ) instock_diff
  WHERE DATE_FORMAT(createdDate, '%Y') = ?
  GROUP BY idIngredient, name, DATE_FORMAT(createdDate, '%Y')
  ORDER BY idIngredient, DATE_FORMAT(createdDate, '%Y');
      
    `,
      [selectedDate],
    );
    return data;
  }
}
