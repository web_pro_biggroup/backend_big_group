import { Controller, Get, Param } from '@nestjs/common';
import { ReportCheckStockService } from './report-check-stock.service';

@Controller('report-check-stock')
export class ReportCheckStockController {
  constructor(private reportCheckStockService: ReportCheckStockService) {}

  @Get('report1/:selectedDate')
  async report1(@Param('selectedDate') selectedDate: string) {
    const data = await this.reportCheckStockService.report1(selectedDate);
    return data;
  }

  @Get('report2/:selectedDate')
  async report2(@Param('selectedDate') selectedDate: string) {
    const data = await this.reportCheckStockService.report2(selectedDate);
    return data;
  }

  @Get('report3/:selectedDate')
  async report3(@Param('selectedDate') selectedDate: string) {
    const data = await this.reportCheckStockService.report3(selectedDate);
    return data;
  }
}
