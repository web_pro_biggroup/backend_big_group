import { Test, TestingModule } from '@nestjs/testing';
import { ReportCheckStockService } from './report-check-stock.service';

describe('ReportCheckStockService', () => {
  let service: ReportCheckStockService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ReportCheckStockService],
    }).compile();

    service = module.get<ReportCheckStockService>(ReportCheckStockService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
