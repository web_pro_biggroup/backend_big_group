import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { SalaryPaymentDetailService } from './salary-payment-detail.service';
import { CreateSalaryPaymentDetailDto } from './dto/create-salary-payment-detail.dto';
import { UpdateSalaryPaymentDetailDto } from './dto/update-salary-payment-detail.dto';

@Controller('salary-payment-detail')
export class SalaryPaymentDetailController {
  constructor(
    private readonly salaryPaymentDetailService: SalaryPaymentDetailService,
  ) {}

  @Post()
  create(@Body() createSalaryPaymentDetailDto: CreateSalaryPaymentDetailDto) {
    return this.salaryPaymentDetailService.create(createSalaryPaymentDetailDto);
  }

  @Get()
  findAll() {
    return this.salaryPaymentDetailService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.salaryPaymentDetailService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateSalaryPaymentDetailDto: UpdateSalaryPaymentDetailDto,
  ) {
    return this.salaryPaymentDetailService.update(
      +id,
      updateSalaryPaymentDetailDto,
    );
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.salaryPaymentDetailService.remove(+id);
  }
}
