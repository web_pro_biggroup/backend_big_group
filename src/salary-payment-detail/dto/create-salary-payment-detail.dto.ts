export class CreateSalaryPaymentDetailDto {
  salaryPaymentDetailItems: {
    empId: number;
    name?: string;
    salary: number;
  }[];
}
