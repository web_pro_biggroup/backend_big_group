import { PartialType } from '@nestjs/mapped-types';
import { CreateSalaryPaymentDetailDto } from './create-salary-payment-detail.dto';

export class UpdateSalaryPaymentDetailDto extends PartialType(
  CreateSalaryPaymentDetailDto,
) {}
