import { Test, TestingModule } from '@nestjs/testing';
import { SalaryPaymentDetailService } from './salary-payment-detail.service';

describe('SalaryPaymentDetailService', () => {
  let service: SalaryPaymentDetailService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SalaryPaymentDetailService],
    }).compile();

    service = module.get<SalaryPaymentDetailService>(
      SalaryPaymentDetailService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
