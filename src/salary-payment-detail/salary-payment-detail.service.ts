import { Injectable } from '@nestjs/common';
import { CreateSalaryPaymentDetailDto } from './dto/create-salary-payment-detail.dto';
import { UpdateSalaryPaymentDetailDto } from './dto/update-salary-payment-detail.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Employee } from 'src/employees/entities/employee.entity';
import { Repository } from 'typeorm';
import { SalaryPaymentDetail } from './entities/salary-payment-detail.entity';
import { SalaryPaymentDetailItem } from './entities/salary-payment-detail-item.entity';

@Injectable()
export class SalaryPaymentDetailService {
  constructor(
    @InjectRepository(SalaryPaymentDetail)
    private salaryPaymentDetailRepository: Repository<SalaryPaymentDetail>,
    @InjectRepository(SalaryPaymentDetailItem)
    private salaryPaymentDetailItemRepository: Repository<SalaryPaymentDetailItem>,
    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>,
  ) {}

  async create(createSalaryPaymentDetailDto: CreateSalaryPaymentDetailDto) {
    const salaryPaymentDetail = new SalaryPaymentDetail();
    salaryPaymentDetail.total = 0;
    salaryPaymentDetail.paymentType = 'Cash';
    salaryPaymentDetail.salaryPaymentDetailItems = [];

    for (const oi of createSalaryPaymentDetailDto.salaryPaymentDetailItems) {
      const employeeStore = await this.employeeRepository.findOne({
        where: { id: oi.empId },
      });

      const salaryPaymentDetailItem = new SalaryPaymentDetailItem();
      salaryPaymentDetailItem.name = employeeStore.name;
      salaryPaymentDetailItem.salary = employeeStore.salary;

      const item =
        await this.salaryPaymentDetailItemRepository.save(salaryPaymentDetail);
      console.log('item' + salaryPaymentDetailItem.id);
      console.log(item);

      salaryPaymentDetail.salaryPaymentDetailItems.push(
        salaryPaymentDetailItem,
      );
      salaryPaymentDetail.total += salaryPaymentDetailItem.salary;
      salaryPaymentDetail.salaryPaymentDetailItems.push(item);
    }
    return this.salaryPaymentDetailRepository.save(salaryPaymentDetail);
  }

  findAll() {
    return this.salaryPaymentDetailRepository.find();
  }

  findOne(id: number) {
    return this.salaryPaymentDetailRepository.findOneOrFail({
      where: { id },
      relations: { salaryPaymentDetailItems: true },
    });
  }

  update(
    id: number,
    updateSalaryPaymentDetailDto: UpdateSalaryPaymentDetailDto,
  ) {
    return `This action updates a #${id} salaryPaymentDetail`;
  }

  async remove(id: number) {
    const deleteOrder = await this.salaryPaymentDetailRepository.findOneOrFail({
      where: { id },
    });
    await this.salaryPaymentDetailRepository.remove(deleteOrder);

    return deleteOrder;
  }
}
