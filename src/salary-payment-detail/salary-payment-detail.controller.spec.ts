import { Test, TestingModule } from '@nestjs/testing';
import { SalaryPaymentDetailController } from './salary-payment-detail.controller';
import { SalaryPaymentDetailService } from './salary-payment-detail.service';

describe('SalaryPaymentDetailController', () => {
  let controller: SalaryPaymentDetailController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SalaryPaymentDetailController],
      providers: [SalaryPaymentDetailService],
    }).compile();

    controller = module.get<SalaryPaymentDetailController>(
      SalaryPaymentDetailController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
