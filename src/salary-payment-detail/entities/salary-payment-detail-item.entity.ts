import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { SalaryPaymentDetail } from './salary-payment-detail.entity';

@Entity()
export class SalaryPaymentDetailItem {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  salary: number;

  @ManyToOne(
    () => SalaryPaymentDetail,
    (salaryPaymentDetail) => salaryPaymentDetail.salaryPaymentDetailItems,
    { onDelete: 'CASCADE' },
  )
  salaryPaymentDetail: SalaryPaymentDetail;
}
