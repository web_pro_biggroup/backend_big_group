import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { SalaryPaymentDetailItem } from './salary-payment-detail-item.entity';

@Entity()
export class SalaryPaymentDetail {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn()
  createdDate: Date;

  //จำนวนเงินสุทธิ
  @Column()
  total: number;

  @Column()
  paymentType: string;

  @OneToMany(
    () => SalaryPaymentDetailItem,
    (salaryPaymentDetailItem) => salaryPaymentDetailItem.salaryPaymentDetail,
  )
  salaryPaymentDetailItems?: SalaryPaymentDetailItem[];
}
