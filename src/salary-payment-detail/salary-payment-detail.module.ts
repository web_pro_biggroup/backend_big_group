import { Module } from '@nestjs/common';
import { SalaryPaymentDetailService } from './salary-payment-detail.service';
import { SalaryPaymentDetailController } from './salary-payment-detail.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Employee } from 'src/employees/entities/employee.entity';
import { SalaryPaymentDetail } from './entities/salary-payment-detail.entity';
import { SalaryPaymentDetailItem } from './entities/salary-payment-detail-item.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      SalaryPaymentDetail,
      SalaryPaymentDetailItem,
      Employee,
    ]), // Import IngredientRepository here
  ],
  providers: [SalaryPaymentDetailService],
  controllers: [SalaryPaymentDetailController],
})
export class SalaryPaymentDetailModule {}
