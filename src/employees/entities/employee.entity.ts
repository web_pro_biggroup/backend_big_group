import { SalaryPaymentDetailItem } from 'src/salary-payment-detail/entities/salary-payment-detail-item.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Employee {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  position: string;

  @Column()
  salary: number;

  @Column({ default: 'noimage.jpg' })
  image: string;

  @Column({ default: false })
  status: boolean;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;
}
