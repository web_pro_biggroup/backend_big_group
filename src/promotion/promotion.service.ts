import { Injectable } from '@nestjs/common';
import { CreatePromotionDto } from './dto/create-promotion.dto';
import { UpdatePromotionDto } from './dto/update-promotion.dto';
import { Promotion } from './entities/promotion.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class PromotionService {
  // lastId: number = 2;
  // promotion: Promotion[] = [
  //   {
  //     id: 1,
  //     condition: 'ซื้อครบ 200 บาท',
  //     startDate: new Date('01/01/2024'),
  //     endDate: new Date('01/01/2024'),
  //   },
  //   {
  //     id: 2,
  //     condition: 'ซื้อครบ 200 บาท',
  //     startDate: new Date('01/01/2024'),
  //     endDate: new Date('01/01/2024'),
  //   },
  // ];
  constructor(
    @InjectRepository(Promotion)
    private promotionRepository: Repository<Promotion>,
  ) {}
  create(createPromotionDto: CreatePromotionDto) {
    return this.promotionRepository.save(createPromotionDto);
  }

  findAll() {
    return this.promotionRepository.find();
  }

  findAllByType(typeId: number) {
    return this.promotionRepository.find({
      where: { type: typeId }, // Assuming 'type' is the relation between Product and Type entities
      order: { id: 'ASC' },
    });
  }

  findOne(id: number) {
    return this.promotionRepository.findOneBy({ id });
  }

  async update(id: number, updatePromotionDto: UpdatePromotionDto) {
    await this.promotionRepository.update(id, updatePromotionDto);
    const Promotion = await this.promotionRepository.findOneBy({ id });
    return Promotion;
  }

  async remove(id: number) {
    const deletePromotion = await this.promotionRepository.findOneBy({ id });
    return this.promotionRepository.remove(deletePromotion);
  }
}
