import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { PromotionCondition } from '../promotionCondition';
import { Order } from 'src/order/entities/order.entity';

@Entity()
export class Promotion {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  condition: string;

  // @Column('json', { default: '{}' })
  // conditionCode: PromotionCondition;

  @Column()
  type: number;

  @Column()
  startDate: Date;

  @Column()
  endDate: Date;

  @OneToMany(() => Order, (order) => order.promotion)
  orders: Order[];
}
