export class CreateExpenseDto {
  id?: number;
  waterUnit: number;
  waterPrice: number;
  elecUnit: number;
  elecPrice: number;
  totalElecPrice: number;
  totalWaterPrice: number;

  rent: number;
  total: number;

  created: Date;
}
