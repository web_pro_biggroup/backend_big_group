import { Test, TestingModule } from '@nestjs/testing';
import { ReportIngredientStoreController } from './report-ingredient-store.controller';

describe('ReportIngredientStoreController', () => {
  let controller: ReportIngredientStoreController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ReportIngredientStoreController],
    }).compile();

    controller = module.get<ReportIngredientStoreController>(
      ReportIngredientStoreController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
