import { Test, TestingModule } from '@nestjs/testing';
import { ReportIngredientStoreService } from './report-ingredient-store.service';

describe('ReportIngredientStoreService', () => {
  let service: ReportIngredientStoreService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ReportIngredientStoreService],
    }).compile();

    service = module.get<ReportIngredientStoreService>(
      ReportIngredientStoreService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
