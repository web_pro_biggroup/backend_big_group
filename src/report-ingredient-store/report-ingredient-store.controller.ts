import { Controller, Get, Param } from '@nestjs/common';
import { ReportIngredientStoreService } from './report-ingredient-store.service';

@Controller('report-ingredient-store')
export class ReportIngredientStoreController {
  constructor(
    private reportIngredientStoreService: ReportIngredientStoreService,
  ) {}
  @Get('report1/:selectedDate')
  async report1(@Param('selectedDate') selectedDate: string) {
    const data = await this.reportIngredientStoreService.report1(selectedDate);
    return data;
  }

  @Get('report2/:selectedDate')
  async report2(@Param('selectedDate') selectedDate: string) {
    const data = await this.reportIngredientStoreService.report2(selectedDate);
    return data;
  }

  @Get('report3/:selectedDate')
  async report3(@Param('selectedDate') selectedDate: string) {
    const data = await this.reportIngredientStoreService.report3(selectedDate);
    return data;
  }
}
