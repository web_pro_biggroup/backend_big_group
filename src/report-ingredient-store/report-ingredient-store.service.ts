import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { OrderIngredient } from 'src/order-ingredient/entities/order-ingredient.entity';
import { OrderIngredientItem } from 'src/order-ingredient/entities/orderItem-ingredient.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ReportIngredientStoreService {
  constructor(
    @InjectRepository(OrderIngredient)
    private ordersIngredientRepository: Repository<OrderIngredient>,
    @InjectRepository(OrderIngredientItem)
    private orderIngredientItemRepository: Repository<OrderIngredientItem>,
  ) {}

  //รายวัน
  async report1(selectedDate: string) {
    const data = await this.orderIngredientItemRepository.query(
      `
      SELECT
      DATE_FORMAT(o.createdDate, '%Y-%m-%d') AS month,
      oi.name AS 'name',
      SUM(oi.qty) AS 'qty'
  FROM
      order_ingredient_item oi
  INNER JOIN
      order_ingredient o ON oi.orderIngredientId = o.id
  WHERE
      DATE_FORMAT(o.createdDate, '%Y-%m-%d') = ?
  GROUP BY
      month, oi.name
  
      `,
      [selectedDate], // ใช้ selectedDate เป็น parameter
    );
    return data;
  }

  //รายเดือน
  async report2(selectedDate: string) {
    const data = await this.orderIngredientItemRepository.query(
      `
      SELECT
      DATE_FORMAT(o.createdDate, '%Y-%m') AS month,
      oi.name AS 'name',
      SUM(oi.qty) AS 'qty'
  FROM
      order_ingredient_item oi
  INNER JOIN
      order_ingredient o ON oi.orderIngredientId = o.id
  WHERE
      DATE_FORMAT(o.createdDate, '%Y-%m') = ?
  GROUP BY
      month, oi.name
  
      `,
      [selectedDate], // ใช้ selectedDate เป็น parameter
    );
    return data;
  }

  //รายปี
  async report3(selectedDate: string) {
    const data = await this.orderIngredientItemRepository.query(
      `
      SELECT
      DATE_FORMAT(o.createdDate, '%Y') AS month,
      oi.name AS 'name',
      SUM(oi.qty) AS 'qty'
  FROM
      order_ingredient_item oi
  INNER JOIN
      order_ingredient o ON oi.orderIngredientId = o.id
  WHERE
      DATE_FORMAT(o.createdDate, '%Y') = ?
  GROUP BY
      month, oi.name
  
      `,
      [selectedDate], // ใช้ selectedDate เป็น parameter
    );
    return data;
  }
}
