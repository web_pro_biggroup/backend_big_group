import { Module } from '@nestjs/common';
import { ReportIngredientStoreController } from './report-ingredient-store.controller';
import { ReportIngredientStoreService } from './report-ingredient-store.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrderIngredient } from 'src/order-ingredient/entities/order-ingredient.entity';
import { OrderIngredientItem } from 'src/order-ingredient/entities/orderItem-ingredient.entity';

@Module({
  imports: [TypeOrmModule.forFeature([OrderIngredient, OrderIngredientItem])],
  controllers: [ReportIngredientStoreController],
  providers: [ReportIngredientStoreService],
})
export class ReportIngredientStoreModule {}
