import { Injectable } from '@nestjs/common';
import { CreateBranchDto } from './dto/create-branch.dto';
import { UpdateBranchDto } from './dto/update-branch.dto';
import { Branch } from './entities/branch.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class BranchService {
  constructor(
    @InjectRepository(Branch)
    private branchRepository: Repository<Branch>,
  ) {}
  create(createBranchDto: CreateBranchDto) {
    return this.branchRepository.save(createBranchDto);
  }

  findAll() {
    return this.branchRepository.find();
  }

  findOne(id: number) {
    return this.branchRepository.findOneBy({ id });
  }

  async update(id: number, updateBranchDto: UpdateBranchDto) {
    await this.branchRepository.update(id, updateBranchDto);
    const user = await this.branchRepository.findOneBy({ id });
    return user;
  }

  async remove(id: number) {
    const deleteUser = await this.branchRepository.findOneBy({ id });
    return this.branchRepository.remove(deleteUser);
  }
}
