export class CreateBranchDto {
  id: number;
  address: string;
  status: 'In Progess' | 'On Hold'; // เปิดอยู่ | ปิดกิจการ
}
