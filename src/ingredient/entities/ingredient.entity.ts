import { History } from 'src/history/entities/history.entity';
import { OrderIngredientItem } from 'src/order-ingredient/entities/orderItem-ingredient.entity';
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Ingredient {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  name: string;
  @Column()
  inStock: number;
  @Column()
  Maximum: number;
  @Column()
  Unit: string;
  @Column()
  createdDate: Date;

  // [key: string]: number | string | Date;

  @ManyToOne(() => History, (history) => history.Ingredients)
  history: History;

  @OneToMany(
    () => OrderIngredientItem,
    (orderIngredientItem) => orderIngredientItem.ingredient,
  )
  orderIngredientItems: OrderIngredientItem[];
}
