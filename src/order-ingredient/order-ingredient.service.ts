import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateOrderIngredientDto } from './dto/create-order-ingredient.dto';
import { UpdateOrderIngredientDto } from './dto/update-order-ingredient.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { OrderIngredient } from './entities/order-ingredient.entity';
import { Repository } from 'typeorm';
import { OrderIngredientItem } from './entities/orderItem-ingredient.entity';
import { IngredientStore } from 'src/ingredient-store/entities/ingredient-store.entity';
import { Ingredient } from 'src/ingredient/entities/ingredient.entity'; // Import Ingredient entity

@Injectable()
export class OrderIngredientService {
  constructor(
    @InjectRepository(OrderIngredient)
    private ordersIngredientRepository: Repository<OrderIngredient>,
    @InjectRepository(OrderIngredientItem)
    private orderIngredientItemRepository: Repository<OrderIngredientItem>,
    @InjectRepository(IngredientStore)
    private ingredientStoreRepository: Repository<IngredientStore>,
    @InjectRepository(Ingredient)
    private ingredientRepository: Repository<Ingredient>,
  ) {}

  async create(createOrderIngredientDto: CreateOrderIngredientDto) {
    const orderIngredient = new OrderIngredient();
    orderIngredient.totalBefore = 0;
    orderIngredient.total = 0;
    orderIngredient.totalAmount = 0;
    orderIngredient.receivedAmount = 0;
    orderIngredient.change = 0;
    orderIngredient.paymentType = 'Cash';
    orderIngredient.orderIngredientItems = [];

    for (const oi of createOrderIngredientDto.orderIngredientItems) {
      const ingredientStore = await this.ingredientStoreRepository.findOne({
        where: { id: oi.ingredientId },
      });

      console.log(ingredientStore);

      if (!oi.ingredientId) {
        throw new Error('IngredientId cannot be null or undefined.');
      }

      if (!ingredientStore) {
        throw new NotFoundException(
          `IngredientStore with id ${oi.ingredientId} not found.`,
        );
      }

      let ingredientFound = await this.ingredientRepository.findOne({
        where: { id: ingredientStore.id },
      });
      console.log(ingredientFound);

      if (!ingredientFound) {
        ingredientFound = new Ingredient();
        ingredientFound.id = ingredientStore.id;
        ingredientFound.name = ingredientStore.name;
        ingredientFound.inStock = 0;
        ingredientFound.Unit = ingredientStore.unit;
      }

      const orderIngredientItem = new OrderIngredientItem();
      orderIngredientItem.ingredient = ingredientStore;
      orderIngredientItem.name = ingredientStore.name;
      orderIngredientItem.acc = ingredientStore.price;
      orderIngredientItem.price = ingredientStore.price * oi.qty;
      orderIngredientItem.qty = oi.qty;
      orderIngredientItem.unit = ingredientStore.unit;

      const item =
        await this.orderIngredientItemRepository.save(orderIngredientItem);
      console.log('item' + orderIngredientItem.ingredient.id);
      console.log(item);

      ingredientFound.inStock += oi.qty; // Increase stock
      await this.ingredientRepository.save(ingredientFound);

      // if (!ingredientFound.orderIngredientItems) {
      //   ingredientFound.orderIngredientItems = [];
      // }
      // ingredientFound.orderIngredientItems.push(orderIngredientItem);
      // await this.ingredientRepository.save(ingredientFound);

      //await this.orderIngredientItemRepository.save(orderIngredientItem);

      orderIngredient.orderIngredientItems.push(orderIngredientItem);
      orderIngredient.total += orderIngredientItem.price;
      orderIngredient.totalAmount += orderIngredientItem.qty;
      orderIngredient.receivedAmount += orderIngredientItem.price;
      orderIngredient.totalBefore += orderIngredientItem.price;
      orderIngredient.orderIngredientItems.push(item);
    }

    return this.ordersIngredientRepository.save(orderIngredient);
  }

  findAll() {
    return this.ordersIngredientRepository.find({
      relations: { orderIngredientItems: true },
      order: {
        id: 'desc',
      },
    });
  }

  findOne(id: number) {
    return this.ordersIngredientRepository.findOneOrFail({
      where: { id },
      relations: { orderIngredientItems: true },
    });
  }

  update(id: number, updateOrderIngredientDto: UpdateOrderIngredientDto) {
    // Implement update logic here
    return `This action updates a #${id} orderIngredient`;
  }

  async remove(id: number) {
    const deleteOrder = await this.ordersIngredientRepository.findOneOrFail({
      where: { id },
    });
    await this.ordersIngredientRepository.remove(deleteOrder);

    return deleteOrder;
  }
}
