import { PartialType } from '@nestjs/mapped-types';
import { CreateOrderIngredientDto } from './create-order-ingredient.dto';

export class UpdateOrderIngredientDto extends PartialType(
  CreateOrderIngredientDto,
) {}
