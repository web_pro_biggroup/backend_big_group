import { Test, TestingModule } from '@nestjs/testing';
import { OrderIngredientService } from './order-ingredient.service';

describe('OrderIngredientService', () => {
  let service: OrderIngredientService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [OrderIngredientService],
    }).compile();

    service = module.get<OrderIngredientService>(OrderIngredientService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
