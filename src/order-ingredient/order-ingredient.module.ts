import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrderIngredient } from './entities/order-ingredient.entity';
import { OrderIngredientItem } from './entities/orderItem-ingredient.entity';
import { IngredientStore } from 'src/ingredient-store/entities/ingredient-store.entity';
import { Ingredient } from 'src/ingredient/entities/ingredient.entity';
import { OrderIngredientService } from './order-ingredient.service';
import { OrderIngredientController } from './order-ingredient.controller';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      OrderIngredient,
      OrderIngredientItem,
      IngredientStore,
      Ingredient,
    ]), // Import IngredientRepository here
  ],
  providers: [OrderIngredientService],
  controllers: [OrderIngredientController],
})
export class OrderIngredientModule {}
