import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  // กำหนดค่า CORS โดยใช้ origin ที่เซตเป็น "http://localhost:5173"
  app.enableCors();

  await app.listen(3000);
}
bootstrap();
