import { Test, TestingModule } from '@nestjs/testing';
import { IngredientStoreService } from './ingredient-store.service';

describe('IngredientStoreService', () => {
  let service: IngredientStoreService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [IngredientStoreService],
    }).compile();

    service = module.get<IngredientStoreService>(IngredientStoreService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
