import { OrderIngredientItem } from 'src/order-ingredient/entities/orderItem-ingredient.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class IngredientStore {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  name: string;
  @Column()
  unit: string;
  @CreateDateColumn()
  createdDate: Date;

  @Column()
  price: number;
  @OneToMany(
    () => OrderIngredientItem,
    (orderIngredientItem) => orderIngredientItem.ingredient, // ปรับเปลี่ยนจาก ingredientStore เป็น ingredient
  )
  orderIngredientItems: OrderIngredientItem[];
}
