import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { IngredientStoreService } from './ingredient-store.service';
import { CreateIngredientStoreDto } from './dto/create-ingredient-store.dto';
import { UpdateIngredientStoreDto } from './dto/update-ingredient-store.dto';

@Controller('ingredient-store')
export class IngredientStoreController {
  constructor(
    private readonly ingredientStoreService: IngredientStoreService,
  ) {}

  @Post()
  create(@Body() createIngredientStoreDto: CreateIngredientStoreDto) {
    return this.ingredientStoreService.create(createIngredientStoreDto);
  }

  @Get()
  findAll() {
    return this.ingredientStoreService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.ingredientStoreService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateIngredientStoreDto: UpdateIngredientStoreDto,
  ) {
    return this.ingredientStoreService.update(+id, updateIngredientStoreDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.ingredientStoreService.remove(+id);
  }
}
