import { Module } from '@nestjs/common';
import { IngredientStoreService } from './ingredient-store.service';
import { IngredientStoreController } from './ingredient-store.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { IngredientStore } from './entities/ingredient-store.entity';

@Module({
  imports: [TypeOrmModule.forFeature([IngredientStore])],
  controllers: [IngredientStoreController],
  providers: [IngredientStoreService],
})
export class IngredientStoreModule {}
