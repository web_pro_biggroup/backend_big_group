import { Injectable } from '@nestjs/common';
import { CreateIngredientStoreDto } from './dto/create-ingredient-store.dto';
import { UpdateIngredientStoreDto } from './dto/update-ingredient-store.dto';
import { IngredientStore } from './entities/ingredient-store.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class IngredientStoreService {
  lastId: number = 0;
  constructor(
    @InjectRepository(IngredientStore)
    private IngredientRepositoryStore: Repository<IngredientStore>,
  ) {}
  async create(createIngredientStoreDto: CreateIngredientStoreDto) {
    const newIngredientStore = this.IngredientRepositoryStore.create(
      createIngredientStoreDto,
    );
    const savedIngredientStore =
      await this.IngredientRepositoryStore.save(newIngredientStore);
    return savedIngredientStore;
  }

  findAll() {
    return this.IngredientRepositoryStore.find();
  }

  findOne(id: number) {
    return this.IngredientRepositoryStore.findOneBy({ id });
  }

  async update(id: number, updateIngredientStoreDto: UpdateIngredientStoreDto) {
    await this.IngredientRepositoryStore.update(id, updateIngredientStoreDto);
    const IngredientStore = await this.IngredientRepositoryStore.findOneBy({
      id,
    });
    return IngredientStore;
  }

  async remove(id: number) {
    const deleteIngredientStore =
      await this.IngredientRepositoryStore.findOneBy({ id });
    return this.IngredientRepositoryStore.remove(deleteIngredientStore);
  }
}
