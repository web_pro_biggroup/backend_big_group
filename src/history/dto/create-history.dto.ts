export class CreateHistoryDto {
  id: number;

  idIngredient: number;

  name: string;

  inStock: number;

  Maximum: number;

  Unit: string;

  createdDate: Date;
}
