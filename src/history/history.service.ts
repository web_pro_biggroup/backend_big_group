import { Injectable } from '@nestjs/common';
import { CreateHistoryDto } from './dto/create-history.dto';
import { UpdateHistoryDto } from './dto/update-history.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { History } from './entities/history.entity';

@Injectable()
export class HistoryService {
  constructor(
    @InjectRepository(History)
    private HistoryRepository: Repository<History>, // Inject History repository
  ) {}
  create(createHistoryDto: CreateHistoryDto) {
    return 'This action adds a new history';
  }

  findAll() {
    return this.HistoryRepository.find();
  }

  findOne(id: number) {
    return `This action returns a #${id} history`;
  }

  update(id: number, updateHistoryDto: UpdateHistoryDto) {
    return `This action updates a #${id} history`;
  }

  remove(id: number) {
    return `This action removes a #${id} history`;
  }
  // HistoryService
  // reportCheck() {
  //   const queryString = `
  //   WITH instock_diff AS (
  //     SELECT
  //       idIngredient,
  //       strftime('%Y-%m', createdDate) AS month,
  //       ABS(inStock - (
  //         SELECT inStock
  //         FROM history h2
  //         WHERE h2.idIngredient = h.idIngredient
  //           AND strftime('%Y-%m', h2.createdDate) = strftime('%Y-%m', h.createdDate)
  //           AND h2.createdDate < h.createdDate
  //         ORDER BY h2.createdDate DESC
  //         LIMIT 1
  //       )) AS diff
  //     FROM history h
  //   )
  //   SELECT
  //     month,
  //     idIngredient,
  //     SUM(diff) AS total_diff
  //   FROM instock_diff
  //   GROUP BY month, idIngredient
  //   ORDER BY month, idIngredient;
  // `;

  //   console.log('Query String:', queryString); // Log query string

  //   return this.HistoryRepository.query(queryString);
  // }
}
