import { Ingredient } from 'src/ingredient/entities/ingredient.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class History {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  idIngredient: number;
  @Column()
  name: string;
  @Column()
  inStock: number;
  @Column()
  Maximum: number;
  @Column()
  Unit: string;
  @Column()
  createdDate: Date;

  @OneToMany(() => Ingredient, (ingredient) => ingredient.history)
  Ingredients: Ingredient[];
}
